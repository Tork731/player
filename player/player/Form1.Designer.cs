﻿namespace player
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.playList = new System.Windows.Forms.ListBox();
            this.btnPlay = new System.Windows.Forms.Button();
            this.labTime = new System.Windows.Forms.Label();
            this.labValue = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.BtnEj = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.slValue = new MB.Controls.ColorSlider();
            this.slTime = new MB.Controls.ColorSlider();
            this.SuspendLayout();
            // 
            // playList
            // 
            this.playList.FormattingEnabled = true;
            this.playList.Location = new System.Drawing.Point(12, 12);
            this.playList.Name = "playList";
            this.playList.Size = new System.Drawing.Size(466, 173);
            this.playList.TabIndex = 0;
            // 
            // btnPlay
            // 
            this.btnPlay.BackgroundImage = global::player.Properties.Resources.play;
            this.btnPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlay.Location = new System.Drawing.Point(12, 262);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(44, 47);
            this.btnPlay.TabIndex = 3;
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // labTime
            // 
            this.labTime.AutoSize = true;
            this.labTime.Location = new System.Drawing.Point(13, 207);
            this.labTime.Name = "labTime";
            this.labTime.Size = new System.Drawing.Size(49, 13);
            this.labTime.TabIndex = 4;
            this.labTime.Text = "00.00.00";
            // 
            // labValue
            // 
            this.labValue.AutoSize = true;
            this.labValue.Location = new System.Drawing.Point(277, 210);
            this.labValue.Name = "labValue";
            this.labValue.Size = new System.Drawing.Size(49, 13);
            this.labValue.TabIndex = 5;
            this.labValue.Text = "00.00.00";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::player.Properties.Resources.stop;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(74, 262);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(46, 47);
            this.button1.TabIndex = 6;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BtnEj
            // 
            this.BtnEj.BackgroundImage = global::player.Properties.Resources._178498;
            this.BtnEj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnEj.Location = new System.Drawing.Point(429, 262);
            this.BtnEj.Name = "BtnEj";
            this.BtnEj.Size = new System.Drawing.Size(49, 47);
            this.BtnEj.TabIndex = 7;
            this.BtnEj.UseVisualStyleBackColor = true;
            this.BtnEj.Click += new System.EventHandler(this.BtnEj_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // slValue
            // 
            this.slValue.BackColor = System.Drawing.Color.Transparent;
            this.slValue.BorderRoundRectSize = new System.Drawing.Size(8, 8);
            this.slValue.LargeChange = ((uint)(5u));
            this.slValue.Location = new System.Drawing.Point(306, 226);
            this.slValue.Name = "slValue";
            this.slValue.Size = new System.Drawing.Size(172, 30);
            this.slValue.SmallChange = ((uint)(1u));
            this.slValue.TabIndex = 2;
            this.slValue.Text = "colorSlider2";
            this.slValue.ThumbRoundRectSize = new System.Drawing.Size(8, 8);
            this.slValue.Value = 100;
            this.slValue.Scroll += new System.Windows.Forms.ScrollEventHandler(this.slValue_Scroll);
            // 
            // slTime
            // 
            this.slTime.BackColor = System.Drawing.Color.Transparent;
            this.slTime.BorderRoundRectSize = new System.Drawing.Size(8, 8);
            this.slTime.LargeChange = ((uint)(5u));
            this.slTime.Location = new System.Drawing.Point(12, 226);
            this.slTime.Maximum = 1000;
            this.slTime.Name = "slTime";
            this.slTime.Size = new System.Drawing.Size(276, 30);
            this.slTime.SmallChange = ((uint)(1u));
            this.slTime.TabIndex = 1;
            this.slTime.Text = "colorSlider1";
            this.slTime.ThumbRoundRectSize = new System.Drawing.Size(8, 8);
            this.slTime.Value = 0;
            this.slTime.Scroll += new System.Windows.Forms.ScrollEventHandler(this.slTime_Scroll);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(515, 321);
            this.Controls.Add(this.BtnEj);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labValue);
            this.Controls.Add(this.labTime);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.slValue);
            this.Controls.Add(this.slTime);
            this.Controls.Add(this.playList);
            this.Name = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.ListBox playList;
        private MB.Controls.ColorSlider slTime;
        private MB.Controls.ColorSlider slValue;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Label labTime;
        private System.Windows.Forms.Label labValue;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button BtnEj;
        private System.Windows.Forms.Timer timer1;
    }
}

#endregion